const test = require("tape");
const mongoose = require("mongoose");
const collection = require("./collection-model");
const connectMongo = require("../helpers/connect-mongo");

connectMongo();

test("collection.find({}) should return an array", async function (t) {
  const collections = await collection.find({});
  const result = Array.isArray(collections);
  const expected = true;
  t.deepEqual(result, expected);
  t.end();
  mongoose.disconnect();
});
