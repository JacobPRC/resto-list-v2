const test = require("tape");
const mongoose = require("mongoose");
const user = require("./user-model");
const connectMongo = require("../helpers/connect-mongo");

connectMongo();

test("user.find({}) should return an array", async function (t) {
  const users = await user.find({});
  const result = Array.isArray(users);
  const expected = true;
  t.deepEqual(result, expected);
  t.end();
  mongoose.disconnect();
});
