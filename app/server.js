const app = require("./index");

const bole = require("bole");

port = process.env.PORT || 5000;

bole.output({ level: "debug", stream: process.stdout });
const log = bole("server");

log.info("server process starting");

app.get("/", function (request, response) {
  console.log("BUTT"); // your JSON
  response.send("HOLE"); // echo the result back
});

app.listen(port, (err) => {
  if (err) {
    log.error("Unable to listen for connections", err);
    process.exit(10);
  }
  log.info("express is listening on http://" + port);
});
