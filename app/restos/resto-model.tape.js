const test = require("tape");
const mongoose = require("mongoose");
const resto = require("./resto-model");
const connectMongo = require("../helpers/connect-mongo");

connectMongo();

test("resto.find({}) should return an array", async function (t) {
  const restos = await resto.find({});
  const result = Array.isArray(restos);
  const expected = true;
  t.deepEqual(result, expected);
  t.end();
  mongoose.disconnect();
});
