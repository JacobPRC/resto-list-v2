const mongoose = require("mongoose");
const dotenv = require("dotenv");

dotenv.config();

module.exports = () => {
  const db = mongoose.connection;

  mongoose.connect(process.env.MONGO, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  });

  db.on("error", console.error.bind(console, "connection error: "));
  db.once("open", () => console.log("Connected successfully", db.host));
};
